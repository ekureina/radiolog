function validate(validation) {
    const sign_in = sign_in_validate(validation.sign_ins)
    const times = times_validate(
            validation.times,
            validation.show_type,
            validation.sign_ins[0].show_name)
    const interupts = interupts_validate(validation.interupts)
    const eas = eas_validate(validation.eas,
        validation.sign_ins[0].name)
    return sign_in && times && interupts && eas
}

function sign_in_validate(sign_in_validation) {
    const first_node = sign_in_validation[0]
    const rows_to_check = sign_in_validation.length
    const outs = sign_in_validation.map(x => x.time_out)
    const ins = sign_in_validation.map(x => x.time_in)
    const names = sign_in_validation.map(x => x.show_name)
    const hostNames = sign_in_validation.map(x => x.name)
    const op_types = sign_in_validation.map(x => x.operator_type)
    const hostName = validate_id_set_zipped("hostName", hostNames)
    const operator_type = validate_name_set_select_zipped("HS", op_types, true)
    const showName = validate_id_set_zipped("showName", names)
    const tIns = validate_id_set_zipped("tIn", ins)
    const tOuts = validate_id_set_zipped("tOut", outs)
    return hostName && operator_type && showName && tIns && tOuts
}

function times_validate(times_validation, show_type, show_name) {
    var correct = true
    for (hour in times_validation) {
        var psa_i = 0
        var donor_i = 0
        var weas_i = 0
        var meas_i = 0
        for (psa in times_validation[hour]) {
            if (psa_i + donor_i + weas_i + meas_i >= 3)
                break
            const psa_data = times_validation[hour][psa]
            switch (psa_data.number) {
                case legal_num:
                    {
                    const to_check = document.getElementById(`id${hour}`)
                    if (to_check.value !== psa_data.time) {
                        to_check.style.backgroundColor = "red"
                        correct = false
                    } else {
                        to_check.style.backgroundColor = "green"
                    }
                    break
                    }
                case meter_num:
                    {
                    const to_check = document.getElementById(`meter_read${hour}`)
                    if (to_check.value != psa_data.reading) {
                        correct = false
                        to_check.style.backgroundColor = "red"
                    } else {
                        to_check.style.backgroundColor = "green"
                    }
                    break
                    }
                case donor_num:
                    {
                    const to_check = document.getElementById(`donor_time_${hour}_${donor_i}`)
                    if (to_check.value !== psa_data.time) {
                        correct = false
                        to_check.style.backgroundColor = "red"
                    } else {
                        to_check.style.backgroundColor = "green"
                    }
                    ++donor_i
                    break
                    }
                case weas_num:
                    {
                    var to_check = document.getElementById(`rwt_time_${hour}_${weas_i}`)
                    if (to_check.value !== psa_data.time) {
                        correct = false
                        to_check.style.backgroundColor = "red"
                    } else {
                        to_check.style.backgroundColor = "green"
                    }
                    ++weas_i
                    break
                    }
                case meas_num:
                    {
                    var to_check = document.getElementById(`rmt_time_${hour}_${meas_i}`)
                    if (to_check.value !== psa_data.time) {
                        correct = false
                        to_check.style.backgroundColor = "red"
                    } else {
                        to_check.style.backgroundColor = "green"
                    }
                    ++meas_i
                    break
                    }
                default:
                    const to_check_num = document.getElementById(`psa_num_${hour}_${psa_i}`)
                    const to_check_val = document.getElementById(`psa_time_${hour}_${psa_i}`)
                    if (to_check_num.value != psa_data.number &&
                        to_check_val.value !== psa_data.time) {
                        to_check_num.style.backgroundColor = "red"
                        to_check_val.style.backgroundColor = "red"
                        correct = false
                    } else {
                        to_check_num.style.backgroundColor = "green"
                        to_check_val.style.backgroundColor = "green"
                    }
                    ++psa_i
            }
        }
    }
    const type = validate_name_set_select(
        times_validation.length,
        "show_type", 
        show_type)
    const name = validate_id_set(
           times_validation.length,
           "show_name",
           show_name)
    return correct && type && name
}

function interupts_validate(interupts_validation) {
    var correct = true
    const offs = interupts_validation.filter(x => x.type === "off")
    const ons = interupts_validation.filter(x => x.type === "on")
    for (off in offs) {
        const station_off = offs[off]
        correct = validate_by_id_regex("OP_NOTES", station_off.notes) && correct
        correct = validate_by_id(`interupt_off_date${off}`, station_off.date) && correct 
        correct = validate_by_id(`interupt_off_time${off}`, station_off.time) && correct
    }

    for (on in ons) {
        const station_on = ons[on]
        correct = validate_by_id(`interupt_on_date${on}`, station_on.date) && correct 
        correct = validate_by_id(`interupt_on_time${on}`, station_on.time) && correct
    }

    return correct
}

function eas_validate(eas_validation, operator_name) {
    var indicies = {
        rwt_incoming: 0,
        rwt_outgoing: 0,
        rmt_incoming: 0,
        rmt_outgoing: 0}

    var correct = true
    const today = new Date()
    const today_str =  today.getMonth()+1 + '/' + today.getDate() + '/' + today.getFullYear()
    const today_str_alt = today.getMonth()+1 + '/' + today.getDate() + '/' + (today.getFullYear()%100)

    for (req_testIt in eas_validation) {
        const req_test = eas_validation[req_testIt]
        const i = indicies[req_test.type]++
        const today = new Date()
        if (req_test.type.split('_')[1] === "incoming") {
            correct = validate_by_id(
                `eas_${req_test.type}_from${i}`,
                req_test.station) && correct
        }
        if (req_test.time !== "") {
            if (!(validate_by_id(`eas_${req_test.type}_date${i}`, today_str) 
                || validate_by_id(`eas_${req_test.type}_date${i}`, today_str_alt))) {
                correct = false // Multiple posibilities, short circuits
            }
        } else {
            correct = validate_by_id(`eas_${req_test.type}_date${i}`, "") && correct
        }
        correct = validate_by_id(
            `eas_${req_test.type}_time${i}`,
            req_test.time) && correct
        correct = validate_by_id(
            `eas_${req_test.type}_op${i}`,
            req_test.time === "" ? "" : operator_name) && correct
    }
    return correct
}

function validate_name_set_select_zipped(set_name, checks) {
    var correct = true
    for (var i = 0; i < checks.length; ++i) {
        const selectbutton = document.getElementsByName(set_name + i)
        if (checks[i] === null) {
            for (var j=0; j < selectbutton.length; ++j) {
                if (selectbutton[j].checked) {
                    correct = false 
                    set_select_bg(selectbutton, "red")
                    break
                } else {
                    set_select_bg(selectbutton, "green")
                }
            }
        } else {
            for (var j=0; j < selectbutton.length; ++j) {
                if ((selectbutton[j].checked &&
                    checks[i] === selectbutton[j].value) ||
                    (!selectbutton[j].checked &&
                    checks[i] !== selectbutton[j].value)) {
                    set_select_bg(selectbutton, "green")
                } else {
                    correct = false
                    set_select_bg(selectbutton, "red")
                    break
                }
            }
        }
    }
    return correct
}

function set_select_bg(elements, color) {
    for (var element = 0; element < elements.length; ++element) {
        elements[element].parentNode.style.backgroundColor = color
    }
}

function validate_name_set_select(set_len, set_name, value=null) {
    if (value === null) {
        value = document.getElementById(set_name + "0").value
    }
    return validate_name_set_select_zipped(set_name, Array(set_len).fill(value))
}

function validate_id_set_zipped(set_id, checks) {
    var correct = true
    for (var i = 0; i < checks.length; ++i) {
        correct = validate_by_id(set_id + i, checks[i]) && correct
    }
    return correct
}

function validate_id_set(set_len, set_id, value=null) {
    if (value === null) {
        value = document.getElementById(set_id + "0").value
    }
    return validate_id_set_zipped(set_id, Array(set_len).fill(value))
}

function validate_by_id(node_id, value) {
    const to_check = document.getElementById(node_id)
    if (to_check.value === value) {
        to_check.style.backgroundColor = "green"
        return true
    } else {
        to_check.style.backgroundColor = "red"
        return false
    }
}

function validate_by_id_regex(node_id, regex) {
    const to_check = document.getElementById(node_id)
    if (regex === null) {
        // Don't influence the validation
        return to_check.style.backgroundColor === "green"
    }
    if (to_check.value.match(regex) !== null) {
        to_check.style.backgroundColor = "green" 
        return true
    } else {
        to_check.style.backgroundColor = "red"
        return false
    }
}

function setValidate(output) {
    document.getElementsByName(output)[0].innerHTML = validate(evaluated_scenario) ? "Pass" : "Fail";
}
