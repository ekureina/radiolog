const legal_num = -1
const donor_num = -2
const meter_num = -3
const weas_num = -4
const meas_num = -5

var evaluated_scenario = null;
var instructions = null;

function request_scenario(loc) {
    var scenarioReq = new XMLHttpRequest()
    scenarioReq.addEventListener("load", eval_scenario)
    scenarioReq.open("GET", loc)
    scenarioReq.send()
}

function eval_scenario() {
    const loaded_scenario = JSON.parse(this.responseText)
    update_html(loaded_scenario)
    const scenario_instructions = loaded_scenario.instructions
    instructions = []
    for (instruction in scenario_instructions) {
        instructions.push(scenario_instructions[instruction].text)
    }
    const hostName = document.getElementById("HostNameSpecifier").value !== "" ?
        document.getElementById("HostNameSpecifier").value : "Host Name"
    const scenario_metadata = loaded_scenario.meta
    var forms = {
        show_type: "show_type" in loaded_scenario.meta ?
            loaded_scenario.meta.show_type : "MUSIC",
        sign_ins: [],
        times: [], 
        interupts: [], 
        eas: []}
    const month = new Date().getMonth()+1
    const day = new Date().getDate()
    for (instruction in scenario_instructions) {
        for (action in scenario_instructions[instruction].actions) {
            var action = scenario_instructions[instruction].actions[action]
            switch (action.type) {
                case "rwt_outgoing":
                case "rwt_incoming":
                case "rmt_outgoing":
                case "rmt_incoming":
                    if (action.type.split("_")[1] === "incoming") {
                        forms.eas.push(
                            {
                                type: action.type,
                                time: action.time,
                                station: action.station
                            })
                    } else {
                        forms.eas.push(
                            {
                                type: action.type,
                                time: action.time,
                            })
                        forms.times[forms.times.length-1].push(
                            {
                                number: (action.type === "rwt_outgoing" ?
                                    weas_num : meas_num),
                                time: action.time
                            })
                    }
                    break
                case "interupt_on":
                    forms.interupts.push(
                        {
                            type: "on",
                            date: month + "/" + day,
                            time: action.time
                        })
                case "sign_on":
                    forms.sign_ins.push({
                        name: hostName,
                        operator_type: ("operator_type" in scenario_metadata) ?
                            scenario_metadata.operator_type : "Host",
                        show_name: loaded_scenario.meta.show_name,
                        time_in: action.time,
                        time_out: ""
                    })
                    break
                case "interupt_off":
                    forms.interupts.push(
                        {
                            type: "off",
                            date: month + "/" + day,
                            time: action.time,
                            notes: new RegExp("^" + month + "/" + day
                            + " " + action.reasons.join(".*")
                            + ".*" + hostName.split(' ').map(x => x[0])
                                .join("")
                            + "$","m")
                        })
                case "sign_off":
                    forms.sign_ins[forms.sign_ins.length-1].time_out = action.time
                    break
                case "legal_id":
                    forms.times.push([{number: legal_num, time:action.time}])
                    break
                case "donor":
                    forms.times[forms.times.length-1].push(
                        {
                            number: donor_num,
                            time: action.time
                        })
                    break
                case "psa":
                    forms.times[forms.times.length-1].push(
                        {
                            number: action.number,
                            time: action.time
                        })
                    break
                case "meter":
                    forms.times[forms.times.length-1].push(
                        {
                            number: meter_num,
                            reading: action.reading
                        })
                    break
                default:
                    console.log(`${action.type} Unimplemented`)
            }
        }
    }
    // Blank spaces
    const num_sign_ins = "sign_ins_requested" in scenario_metadata ?
        loaded_scenario.meta.sign_ins_requested : 3
    for (var i = forms.sign_ins.length; i < num_sign_ins; ++i) {
        forms.sign_ins.push({
                        name: "",
                        operator_type: null,
                        show_name: "",
                        time_in: "",
                        time_out: ""
        })
    }
    const ons = forms.interupts.filter(x => x.type === "on")
    const offs = forms.interupts.filter(x => x.type === "off")
    const num_interupts = "interupts_requested" in scenario_metadata ?
        scenario_metadata.interupts_requested : 3 
    for (var i = ons.length; i < num_interupts; ++i) {
        forms.interupts.push({
            type: "on",
            date: "",
            time: "",
        })
    }
    for (var i = offs.length; i < num_interupts; ++i) {
        forms.interupts.push({
            type: "off",
            date: "",
            time: "",
            notes: null
        })
    }
    const eas_types = [
        "rwt_outgoing",
        "rwt_incoming",
        "rmt_outgoing",
        "rmt_incoming"
    ]
    const eas_lines = "eas_lines_requested" in scenario_metadata ?
        scenario_metadata.eas_lines_requested : 3
    for (var i = 0; i < eas_types.length; ++i) { 
        for (var j = forms.eas.filter(x => x.type === eas_types[i]).length;
            j < eas_lines; ++j) {
            forms.eas.push({
                type: eas_types[i],
                time: "",
                station: ""
            })
        }
    }

    evaluated_scenario = forms
}

var instruction_number = 0

function get_prev_instruction() {
    --instruction_number
    if (instruction_number < 0) {
        instruction_number = 0
    }
    return instructions[instruction_number]
}

function get_next_instruction() {
    ++instruction_number
    if (instruction_number >= instructions.length) {
        instruction_number = instructions.length
        return "Finished Instructions!"
    }
    return instructions[instruction_number]
}

function print_evaluated_scenario() {
    console.log(evaluated_scenario)
}
