function update_html(scenario_json, html_to_edit = document) {
    html_to_edit.getElementById("Instruction").innerHTML = 
        scenario_json.instructions[0].text
    html_to_edit.getElementById("title").innerHTML = "FCC Log Helper: "
        + scenario_json.meta.display_name + " (" + 
        scenario_json.meta.show_name + ")"
    html_to_edit.getElementById("main_header").innerHTML =
        html_to_edit.getElementById("title").innerHTML
    load_sign_in("sign_ins_requested" in scenario_json.meta ?
        scenario_json.meta.sign_ins_requested : 3, html_to_edit)
    load_interupts("interupts_requested" in scenario_json.meta ?
        scenario_json.meta.interupts_requested : 3, html_to_edit)
    const start_time = scenario_json.meta.start_time
    load_hours("show_hours" in scenario_json.meta ?
        scenario_json.meta.show_hours : 2,
        "special_slots" in scenario_json.meta ? 
        scenario_json.meta.special_slots : [],
        start_time.split(":")[0],
        start_time.slice(-2), html_to_edit)
    load_eas_tables("eas_lines_requested" in scenario_json.meta ?
        scenario_json.meta.eas_lines_requested : 3, html_to_edit)
    const opNotes = document.getElementById("OP_NOTES")
    opNotes.style.backgroundColor = ""
    opNotes.value = ""
}

function load_sign_in(lines, html_to_edit = document) {
    var sign_in_sheet = html_to_edit.getElementById("sign_in")
    sign_in_sheet.innerHTML = "<tr><th>Print Name</th><th>Host" +
        "<th>Sub</th><th>Show Name</th><th>Time In</th><th>Time Out</th></tr>"
    for (var i = 0; i < lines; ++i) {
        sign_in_sheet.innerHTML += "<tr>"
            + '<td><input type="text" id="hostName' + i + '"></td>'
            + '<td><input type="checkbox" name="HS' + i + '" value="Host"></td>'
            + '<td><input type="checkbox" name="HS' + i + '" value="Sub"></td>'
            + '<td><input type="text" id="showName' + i +'"></td>'
            + '<td><input type="text" id="tIn' + i + '"></td>'
            + '<td><input type="text" id="tOut' + i + '"></td></tr>'
    }
}

function load_interupts(lines = 3, html_to_edit = document) {
    var interupts = html_to_edit.getElementById("interupts_table")
    var interuptHTML = '<tr><th colspan="6">INTERUPTIONS TO CARRIER</th></tr>'+
            '<tr><td /><td>DATE</td><td>TIME</td><td /><td>DATE</td>' +
            '<td>TIME</td>'
    for (var i = 0; i < lines; ++i) {
        interuptHTML += "<tr><td>CARRIER OFF:</td>" +
            `<td><input id="interupt_off_date${i}" type='text'></td>` +
            `<td><input id="interupt_off_time${i}" type='text'></td>` +
            "<td>CARRIER ON:</td>" +
            `<td><input id="interupt_on_date${i}" type='text'></td>` +
            `<td><input id="interupt_on_time${i}" type='text'></td></tr>`

    }
    interupts.innerHTML = interuptHTML
}

function load_hours(lines = 2, special_metadata, start_hour, day_half, html_to_edit = document) {
    var exceptions = []
    for (var i = 0; i < lines; ++i) {
        exceptions.push([])
    }
    for (slot in special_metadata) {
        const exception = special_metadata[slot]
        exceptions[exception.hour-1].push(exception)
    }
    var hours = html_to_edit.getElementById("times")
    var hour_table = ""
    var exception_idex = 0;
    for (var i = 0; i < lines; ++i) {
        hour_table += `<table id="time_log${i}"><thead><tr>` + 
            `<th colspan="3">SHOW NAME <input id="show_name${i}" ` +
            'type="text"></th></tr><tr><td rowspan="2">TIME ' +
            'SCHEDULED</td><th>SHOW TYPE</th><td rowspan="2">TIME AIRED' +
            `</td></tr><tr><td><input name="show_type${i}" type="checkbox" ` +
            `value="MUSIC">MUSIC/<input name="show_type${i}" type="checkbox" ` +
            'value="PA">PA</td></tr></thead>' +
            `<tbody><tr><td>${start_hour}:00 ${day_half}</td><td>` +
            `ID/DIS</td><td><input id="id${i}" type="text"></td></tr>`

        ++start_hour
        if (start_hour === 12) {
            if (day_half === "PM") {
                day_half = "AM"
            }
            else {
                day_half = "PM"
            }
        } else if (start_hour === 13) {
            start_hour = 1
        }
        const meter_read = exceptions[i].find(x => x.type == "meter")
        if (meter_read !== undefined) {
            hour_table += `<tr><td>${meter_read.time}</td><td><input id="meter_read${i}" type="text"> W</td><td /></tr>`
        }
        var psa_num = 0;
        var donor_num = 0;
        var weas_num = 0;
        var meas_num = 0;
        for (var j = 0; j < 3; ++j) {
            const donor_announcement = exceptions[i].find(x => x.type == "donor" && x.slot == j+1)
            const send_weas = exceptions[i].find(x => x.type == "rwt_outgoing" && x.slot == j+1)
            const send_meas = exceptions[i].find(x => x.type == "rmt_outgoing" && x.slot == j+1)
            if (donor_announcement !== undefined) {
                hour_table += '<tr><td>' + donor_announcement.time + '</td>' +
                    '<td><del>PSA</del> ' + donor_announcement.number +
                    `</td><td><input id="donor_time_${i}_${donor_num}" type="text">` +
                    '</td></tr>'
                ++donor_num
            }
            else if (send_weas !== undefined) {
                hour_table += '<tr><td>' + send_weas.time + '</td>' +
                    '<td><del>PSA</del> RWT' +
                    `</td><td><input id="rwt_time_${i}_${weas_num}" type="text">` +
                    '</td></tr>'
                ++weas_num
            }
            else if (send_meas !== undefined) {
                hour_table += '<tr><td>' + send_meas.time + '</td>' +
                    '<td><del>PSA</del> RMT' +
                    `</td><td><input id="rmt_time_${i}_${meas_num}" type="text">` +
                    '</td></tr>'
                ++meas_num
            }
            else {
                hour_table += `<tr><td /><td>PSA <input id="psa_num_${i}_${psa_num}"` +
                    `type="text"></td><td><input id="psa_time_${i}_${psa_num}"` +
                    'type="text"></td></tr>'
                ++psa_num
            }
        }
        hour_table += "</tbody></table><p>"
    }
    hours.innerHTML = hour_table
}

function load_eas_tables(lines=3) {
    const eas_div = document.getElementById('eas')
    const eas_types = [
        "rwt_incoming",
        "rwt_outgoing",
        "rmt_incoming",
        "rmt_outgoing"]
    var easHTML = ''
    for (typeIt in eas_types) {
        const type = eas_types[typeIt]
        const displayType = type.split('_')[0] === "rwt" ? "WEEKLY": "MONTHLY"
        const displayDir = type.split('_')[1] === "incoming" ? "RECIEVED" :
            "SENT BY KUCI"
        easHTML += `<table><tr><th colspan="4">${displayType} EAS ` +
            `TESTS ${displayDir}</th></tr><tr>`
        if (displayDir === "RECIEVED") {
            easHTML += '<td>STATION SENDING TEST</td>'
        }
            easHTML += '<td>DATE</td><td>TIME</td><td>PRINT NAME</td></tr>'
        for (var i = 0; i < lines; ++i) {
            if (displayDir === "RECIEVED") {
                easHTML += `<tr><td><input id="eas_${type}_from${i}" type="text"></td>`
            } else {
                easHTML += '<tr>'
            }
            easHTML += `<td><input id="eas_${type}_date${i}" type="text"></td>` +
                `<td><input id="eas_${type}_time${i}" type="text"></td>` +
                `<td>OP: <input id="eas_${type}_op${i}" type="text"></td></tr>`
        }
        easHTML += '</table><p>'
        eas_div.innerHTML = easHTML
    }
}
