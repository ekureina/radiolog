import os, json, stat
from pathlib import Path

def main():
    js_text = "function create_selectors(html_to_edit=document) {" + \
        'const selector_div = html_to_edit.getElementById("selector");' + \
        'selector_div.innerHTML=\'<select id="scenario_select">'
    for scenario in os.listdir(Path("../scenarios")): 
        scenario_config = ""
        with open(Path("../scenarios") / Path(scenario)) as json_file:
            scenario_config = json.load(json_file)["meta"]
        display_name = scenario_config["display_name"].replace("'", "\\'")
        show_name = scenario_config["show_name"].replace("'", "\\'")
        sani_scenario = scenario.replace("'", "\\'")
        js_text += f'<option value="{sani_scenario}">' + \
            f'{display_name}' + \
            f'({show_name})</option>'
    js_text += "</select>';}\n"
    js_text += "function random_scenario(){" +\
            "const opts=document.getElementById('scenario_select').options;"+\
            "return opts[Math.floor(Math.random()*opts.length)].value;}"
    with open(Path("../frontend/selector.js"), 'w') as output:
        output.write(js_text)
    os.chmod(Path("../frontend/selector.js"), \
            stat.S_IRWXU | \
            stat.S_IRGRP | stat.S_IXGRP | \
            stat.S_IROTH | stat.S_IXOTH)

if __name__ == "__main__":
    main()
