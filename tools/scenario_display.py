#!/usr/bin/env python3
import json

def main():
    scenario_filename = input()
    while scenario_filename != "":
        with open(scenario_filename) as scenario_file:
            scenario_json = json.loads(scenario_file.read())
            output_scenario(scenario_json)

        scenario_filename = input()

def output_scenario(scenario_json):
    scenario_metadata = scenario_json["meta"]
    print(f'{scenario_metadata["display-name"]}: {scenario_metadata["show-name"]}' if "display-name" in scenario_metadata else scenario_metadata["show-name"])
    print(f"Starts at: {scenario_metadata['start-time']}")
    print(f"Lasts for {scenario_metadata['show-hours']} hours")
    print(f"Consists of:{get_specials(scenario_metadata)}")
    print("Instructions:")
    for number, instruction in enumerate(scenario_json["instructions"], start=1):
        print(f'{number}. {instruction["text"]}\n')

def get_specials(scenario_meta):
    specials = ""
    for special in scenario_meta["special-slots"]:
        specials += f"\n{special['type']} in hour {special['hour']}" + (f" at {special['time']}" if "time" in special else "")
    return specials
        

if __name__ == "__main__":
    main()
